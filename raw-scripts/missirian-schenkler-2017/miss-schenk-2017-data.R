## ----eval=FALSE----------------------------------------------------------
#> gadm.study.countries<-duplicator::combineGADM(duplicator::study.countries$iso3)


## ----fig.keep='all', out.width="90%", fig.align='center'-----------------
gadm.study.countries<-sf::st_read(system.file("shape/study_countries.shp", package="duplicator"))
plot(gadm.study.countries["NAME_0"])


## ----results='hide'------------------------------------------------------
country.crop.calendar<-cropcalStats(duplicator::maize.cal.plant,
                                    duplicator::maize.cal.harv,
                                    gadm.study.countries,
                                    "GID_0")


## ----results='hide'------------------------------------------------------
country.crop.calendar[1:15]


## ------------------------------------------------------------------------
country.crop.calendar[is.na(plant.mode)|is.na(harvest.mode),GID_0]


## ----results='hide'------------------------------------------------------
country.crop.calendar<-cropcalStats(duplicator::maize.cal.plant.fill,
                                    duplicator::maize.cal.harv.fill,
                                    gadm.study.countries,
                                    "GID_0")
country.crop.calendar[is.na(plant.mode)]


## ------------------------------------------------------------------------
country.crop.months <- cropCal(country.crop.calendar,
                               "GID_0",
                               "plant.mode",
                               "harvest.mode")


## ------------------------------------------------------------------------
# Check coordinate system, extent, and resolution of rasters
raster::crs(duplicator::maize.frac)
raster::crs(duplicator::monthly.temp)

raster::extent(duplicator::maize.frac)
raster::extent(duplicator::monthly.temp)

raster::res(duplicator::maize.frac)
raster::res(duplicator::monthly.temp)

res.factor<-(raster::res(duplicator::monthly.temp)/raster::res(duplicator::maize.frac))[1]
maize.frac.agg<-raster::aggregate(duplicator::maize.frac, fact = res.factor, fun = mean)


## ------------------------------------------------------------------------
maize.frac.adj<- maize.frac.agg*raster::area(maize.frac.agg)
rm(maize.frac.agg)


## ----results='hide'------------------------------------------------------
maize.frac.adj[is.na(maize.frac.adj)]<-0
country.temp.month<-weightedStack(duplicator::monthly.temp,
                                  maize.frac.adj,
                                  gadm.study.countries,
                                  "GID_0",
                                  var.name = "month.year",
                                  value.name = "mean.temp")


## ----results='hide'------------------------------------------------------
monthly.temp.sq <- duplicator::monthly.temp ** 2

country.temp.month.sq<-weightedStack(monthly.temp.sq,
                                  maize.frac.adj,
                                  gadm.study.countries,
                                  "GID_0",
                                  var.name = "month.year",
                                  value.name = "mean.temp.sq")
country.temp.month<-merge(country.temp.month, country.temp.month.sq,
                          by=c("GID_0", "month.year"))

## ----message=FALSE, warning=FALSE, include=FALSE-------------------------
rm(country.temp.month.sq, monthly.temp.sq)
gc()


## ----results='hide'------------------------------------------------------
raster::extent(maize.frac.adj)
raster::extent(duplicator::monthly.precip)

country.precip.month <- weightedStack(duplicator::monthly.precip,
                                      maize.frac.adj,
                                      gadm.study.countries,
                                      "GID_0",
                                      var.name = "month.year",
                                      value.name = "mean.precip")

monthly.precip.sq <- duplicator::monthly.precip ** 2

country.precip.month.sq<-weightedStack(monthly.precip.sq,
                                        maize.frac.adj,
                                        gadm.study.countries,
                                        "GID_0",
                                        var.name = "month.year",
                                        value.name = "mean.precip.sq")

country.precip.month<-merge(country.precip.month, country.precip.month.sq,
                          by=c("GID_0", "month.year"))



## ------------------------------------------------------------------------
country.climate.month<-merge(country.temp.month, country.precip.month,
                             by=c("GID_0", "month.year"))


## ----message=FALSE, warning=FALSE, include=FALSE-------------------------
rm(country.temp.month, country.precip.month,
   monthly.precip.sq, maize.frac.adj)
gc()


## ------------------------------------------------------------------------
country.climate.month[,month:=substr(month.year,1,3)][,
                    year:=as.numeric(substr(month.year,5,8))][,month.year:=NULL]

# Create an index of rows in the monthly temperature data we need to keep
keeps = sort(country.climate.month[country.crop.months, on=.(GID_0, month), which=TRUE, nomatch=0])

# Subset the monthly temperature data based on the index
country.climate.month<-country.climate.month[keeps]
data.table::setcolorder(country.climate.month, c("GID_0","year","month"))
names(country.climate.month)[1]<-"iso3"

rm(country.crop.calendar, country.crop.months)


## ------------------------------------------------------------------------
# First the annual averages
country.climate.year <-
  country.climate.month[, .(temp.mean = mean(mean.temp),
                            temp.sq.mean = mean(mean.temp.sq),
                            precip.mean = mean(mean.precip),
                            precip.sq.mean = mean(mean.precip.sq)),
                        by = .(iso3, year)]

# Now the baselines and anomalies
country.climate.year[
  ,temp.base := mean(temp.mean), by = iso3][
   ,temp.anom := (temp.mean - temp.base)][
    ,temp.sq.base := mean(temp.sq.mean), by = iso3][
     ,temp.sq.anom := (temp.sq.mean - temp.sq.base)][
      ,precip.base := mean(precip.mean), by = iso3][
       ,precip.anom := (precip.mean - precip.base)][
        ,precip.sq.base := mean(precip.sq.mean), by = iso3][
         ,precip.sq.anom := (precip.sq.mean - precip.sq.base)]

rm(country.climate.month)


## ------------------------------------------------------------------------
oecd<-unique(commonCodes::commonCodes[oecd=="1",.(gadm.name,iso3)])
eu<-unique(commonCodes::commonCodes[eu=="1",.(gadm.name,iso3)])


## ------------------------------------------------------------------------
apps<-untools::getAsylumStatus()
names(apps)


## ------------------------------------------------------------------------
apps<-apps[iso3.asylum %in% eu$iso3]
apps<-apps[!(iso3.origin %in% c(eu$iso3, oecd$iso3))]
apps<-apps[iso3.origin %in% study.countries$iso3][year %in% seq(2000,2014,1)]
apps<-apps[,.(apps=sum(applied.during)), by=.(origin, iso3.origin, year)][apps>0][,apps:=log(apps)]


## ------------------------------------------------------------------------
apps[,apps.base:=mean(apps, na.rm=TRUE), by=iso3.origin]
apps[,apps.anom:=(apps-apps.base)]


## ------------------------------------------------------------------------
model.dat <- merge(
  apps,
  country.climate.year,
  by.x = c("iso3.origin", "year"),
  by.y = c("iso3", "year"),
  all.x = TRUE
)
names(model.dat)[c(1, 3)] <- c("iso3", "name")
table(is.na(model.dat))[1]
dim(model.dat)[1] * dim(model.dat)[2]

data.table::setcolorder(
  model.dat,
  c(
    "name",
    "iso3",
    "year",
    "apps",
    "apps.anom",
    "apps.base",
    "temp.mean",
    "temp.anom",
    "temp.base",
    "temp.sq.mean",
    "temp.sq.anom",
    "temp.sq.base",
    "precip.mean",
    "precip.anom",
    "precip.base",
    "precip.sq.mean",
    "precip.sq.anom",
    "precip.sq.base"
  )
)

