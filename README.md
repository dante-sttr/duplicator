
<!-- README.md is generated from README.Rmd. Please edit that file -->

# duplicator <img src='man/figures/logo.png' align="right" width="150" />

In addition to providing a curated catalogue of data sets and analytic
tools tailored to environment-security research, one of the primary
goals of the DANTE Project is to present use cases that set out to
replicate methodologies employed by modern high impact peer reviewed
studies in human geography, political science, and environmental
science. While developing the replication studies we will provide
several tools designed to make advanced analytical techniques more
accessible to begginer and intermediate users.

The ability to parse leading peer reviewed research, comprehend their
underlying methodologies, and adapt them to personal needs is a major
barrier to early-career scientists with high technical aspirations. This
already difficult task is made even more challenging by publications
with inadequate written methods plagued by poorly conveyed data
processing procedures, insufficient details regarding software and
package use, and a lack of specific arguments used for statistical
modeling. All these issues would be remedied if researchers were
required to provide their underlying code along with submissions. Not
only would this provide total transparency, but it would serve as a
teaching tool for graduate students and early career scientists.

Sadly, replication is one of the most overlooked aspects of the
scientific process. Only recently have journals began to require
underlying data used in analysis be submitted alongisde the manuscript.
Even fewer journals require code used to generate analysis be submitted.
This is especially problematic in environment-security research where
the signals are notoriously weak, and policy decisions based on research
may have lasting global impacts. We developed `duplicator` with the goal
of promoting reproducible open-access research. The replication studies
developed by the DANTE platform are contained entirely within the
`duplicator` package. All data sets, analysis, and written documents can
be reproduced using the functions, data, and vignettes embedded within
the package. Moreover, all data acquisition and pre-processing steps can
be re-produced using our raw data scripts. The end result is a
completely transparent and open-source research and teaching platform.

# Usage

## Installation

You can install the current version of `duplicater` from
[GitLab](https://gitlab.com) by running the following command in your R
console:

``` r
devtools::install_gitlab("dante-sttr/duplicator", dependencies = TRUE, build_vignettes = TRUE)
```

### Installation With Windows

To install R packages over Git on a Windows system, you must install
Rtools first. The latest version of Rtools is available
[here](https://cran.r-project.org/bin/windows/Rtools/). Furthermore, you
may experience difficulty installing R packages over Git if you utilize
a Windows machine on a network with Active Directory or shared network
drives. To enable proper package installation under these circumstances
please follow [this
guide](https://dante-sttr.gitlab.io/dante-vignettes/windows-pkg-inst/windows-pkg-inst.html).

## Accessing Package Features

### Available Replication Studies

  - Missirian and Schenkler’s 2017: [*Asylum applications respond to
    temperature
    fluctuations*](http://science.sciencemag.org/content/358/6370/1610).
      - [Part I:
        Introduction](https://dante-sttr.gitlab.io/duplicator/articles/miss-schenk-2017-intro.html)
      - [Part II: Data
        Processing](https://dante-sttr.gitlab.io/duplicator/articles/miss-schenk-2017-data.html)
      - [Part III: Data
        Visualization](https://dante-sttr.gitlab.io/duplicator/articles/miss-schenk-2017-vis.html)
      - [Part IV: Statistical
        Modeling](https://dante-sttr.gitlab.io/duplicator/articles/miss-schenk-2017-model.html)

### Functions

A full list of available functions developed for `duplicator`
replication studies can be accessed through the online [reference
manual](https://dante-sttr.gitlab.io/duplicator/reference/index.html).
The underlying code for functions developed for use in `duplicator`
replication studies can be found in the GitLab repository
[here](https://gitlab.com/dante-sttr/duplicator/tree/master/R).

### Datasets

Nearly all data sets used for `duplicator` replication studies are
available as exported objects and are fully documented with title,
description, minimal metadata, url for hosted location, and official
citation when available. To retrieve dataset documentation, call
`help()` on the dataset in question, or browse the full list of
available data sets in the `duplicator` [reference
manual](https://dante-sttr.gitlab.io/duplicator/reference/index.html).
Raw code used to produce embedded datasets can be accessed
[here](https://gitlab.com/dante-sttr/duplicator/tree/master/raw-data).

### Vignettes

You can browse available vignettes and access the raw `.Rmd` files by
using the `browseVignettes("duplicator")` command in your console.
`duplicator` replication vignettes can also be accessed through the
links below or the online [reference
materials](https://dante-sttr.gitlab.io/duplicator/articles).
