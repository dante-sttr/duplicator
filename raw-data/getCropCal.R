  if(!file.exists("raw-data/Maize.crop.calendar.nc.gz")){
  crop.cal.half<-download.file("https://nelson.wisc.edu/sage/data-and-models/crop-calendar-dataset/netcdf/5min/Maize.crop.calendar.nc.gz",
                               destfile = "raw-data/Maize.crop.calendar.nc.gz")
  R.utils::gunzip("raw-data/Maize.crop.calendar.nc.gz")
}

maize.cal.plant<-raster::raster("raw-data/Maize.crop.calendar.nc", varname = "plant")
maize.cal.harv<-raster::raster("raw-data/Maize.crop.calendar.nc", varname = "harvest")

maize.cal.plant<-maize.cal.plant*1
maize.cal.harv<-maize.cal.harv*1

usethis::use_data(maize.cal.plant, overwrite = TRUE, compress = 'xz')
usethis::use_data(maize.cal.harv, overwrite = TRUE, compress = 'xz')

file.remove(c("raw-data/Maize.crop.calendar.nc"))

# Use the extrapolated data

if(!file.exists("raw-data/Maize.crop.calendar.nc.gz")){
  crop.cal.half<-download.file("https://nelson.wisc.edu/sage/data-and-models/crop-calendar-dataset/netcdf/5min/Maize.crop.calendar.fill.nc.gz",
                               destfile = "raw-data/Maize.crop.calendar.fill.nc.gz")
  R.utils::gunzip("raw-data/Maize.crop.calendar.fill.nc.gz")
}

maize.cal.plant.fill<-raster::raster("raw-data/Maize.crop.calendar.fill.nc", varname = "plant")
maize.cal.harv.fill<-raster::raster("raw-data/Maize.crop.calendar.fill.nc", varname = "harvest")

maize.cal.plant.fill<-maize.cal.plant.fill*1
maize.cal.harv.fill<-maize.cal.harv.fill*1

usethis::use_data(maize.cal.plant.fill, overwrite = TRUE, compress = 'xz')
usethis::use_data(maize.cal.harv.fill, overwrite = TRUE, compress = 'xz')

file.remove(c("raw-data/Maize.crop.calendar.fill.nc"))
